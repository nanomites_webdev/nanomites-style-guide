![tepuktangan](http://cl.ly/J4Gp/1345836278029.gif)

#Nanomites, Inc's Style Guide

Repo ini berisi referensi untuk menulis code untuk proyek proyek yang ada di Nanomites, Inc.  

untuk saat ini panduan tersedia untuk:

* Editor Settings
* HTML
* CSS  

:)

------

##Editor Setting

Editor yang dipakai adalah [Sublime Text 2](http://www.sublimetext.com/), dengan setting sebagai berikut:

	{
		"margin": 4,
		"tab_size": 4,
		"auto_indent": true,
		"default_encoding": "UTF-8",
		"default_line_ending": "system",
	}

cukup copy - paste potongan code diatas ke Preferences > Settings - User

------

##HTML Style Guide

###DOCTYPE
Selalu gunakan `DOCTYPE` dengan benar. `DOCTYPE` yang direkomendasikan adalah html5 doctype, yaitu 

	<!DOCTYPE html>

###Head
elemen `<head>` harus berisi:
* `<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">`
* `<title>`

selalu gunakan [CSS Reset](https://gist.github.com/3636667#file_reset_min.css) dan [html5.js](http://html5shim.googlecode.com/svn/trunk/html5.js) pada setiap proyek.
bila perlu, pasang satu file CSS khusus untuk styling pada browser IE.

contoh:

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Nanomites</title>
		<meta name="description" content="nanomites test page">
		<meta name="author" content="author">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" type="text/css" href="css/reset.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<!--[if IE]>
		<link rel="stylesheet" href="css/style-ie.css">
	    <![endif]-->
		<!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	</head>

###Markup
Markup berpengaruh pada SEO, sehingga code yang rapi akan membantu menaikkan rating SEO.  

###Cek !
Cek halaman html dengan CSS dan Javascript disabled, untuk mengetahui markup error

###Referensi
* [blogs.msdn.com/b/jennifer/archive/2011/08/01/html5-part-1-semantic-markup-and-page-layout](http://blogs.msdn.com/b/jennifer/archive/2011/08/01/html5-part-1-semantic-markup-and-page-layout.aspx)
* [net.tutsplus.com/tutorials/html-css-techniques/how-to-make-all-browsers-render-html5-mark-up-correctly-even-ie6/](http://net.tutsplus.com/tutorials/html-css-techniques/how-to-make-all-browsers-render-html5-mark-up-correctly-even-ie6/)
* [impressivewebs.com/html5-syntax-style/](http://www.impressivewebs.com/html5-syntax-style/)

------

##CSS Style Guide

###Dasar
Syntax CSS yang digunakan adalah CSS2.1 & CSS3, dengan aturan dasar sebagai berikut:  

* Taruh spasi setelah `:` pada setiap deklarasi properti
* Taruh spasi sebelum `{` pada setiap deklarasi selector
* Deklarasikan warna dalam format Hexa (`#FFF`)
* Tulis dokumentasi dalam file CSS

###Reset
CSS Reset yang direkomendasikan:
<script src="https://gist.github.com/3636667.js?file=reset-min.css"></script>

###Font
Gunakan satuan `em` untuk mengatur besaran font.
Apabila menggunakan CSS3 `@font-face`, embed dari google fonts.

###Format File
File CSS disatukan kedalam satu file untuk meminimalisir overhead, kecuali file CSS Reset & IE Specific CSS.

###Do & Don't

####Do
1. Minify file CSS untuk production site
2. Gunakan CSS Shorthand untuk meminimalisir size file.
3. Cross Browser CSS.

####Don't
* Jangan menggunakan `@import` dalam file CSS.

###Contoh file CSS

	/*
	 * CSS For Nanomites.Inc Website
	 *
	*/

	/* Global Styles */

	* {
		-webkit-transition: all 0.3s ease-in-out; 
		-moz-transition: all 0.3s ease-in-out; 
		-o-transition: all 0.3s ease-in-out; 
		transition: all 0.3s ease-in-out;
	}

	html {
		height: 100%;
	}

	body {
		height: 100%;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		font-size: 1em;
		padding: 0;
		margin: 0;
		color: #FFF;
		background: #00bff2;
	}

####Lebih Lanjut
* Jangan terlalu banyak menggunakan animasi via CSS, karena alasan kompabilitas browser
* Gunakan selector CSS2.1, karena alasan kompabilitas browser

####Testing
Test file CSS yang telah dibuat di Browser berikut:  

* IE6 - IE9
* Mozilla Firefox 9 >
* Google Chrome - jika memungkinkan gunakan Google Chrome Release, bukan Beta / Dev Channel
* Safari